# Bcrypted password encoding and checking

## Rationale

INCEPTION uses a backed SQL database (MySQL) that we want to modify to create users circumventing the Web UI.
The passwords in the `users` table are however already salted and hashed with Bcrypt.

## Background

Bcrypt produces a string that consists of three fields, delimited by "$":
 
+  2a identifies the bcrypt algorithm version that was used.
+  10 is the cost factor. (12 or higher is recommended...)
+  The salt and the cipher text, concatenated and encoded in a modified Base-64.

The first 22 characters decode to a 16-byte value for the salt. The remaining characters are cipher text to be compared for authentication.

## Java Spring Framework

INCEPTION uses the Java Spring Framework (and therefore its implementation to encode and verify passwords).

We can use [`SpringPasswordEncoder`](https://github.com/raags/SpringPasswordEncoder) to get hashes for our passwords.

### Install

```
git clone git@github.com:raags/SpringPasswordEncoder.git
cd SpringPasswordEncoder/
mvn package
```

### Usage

Encode a password:

```
java -jar target/spring-password-0.1.0.jar -e
```

Verify a password with a given hash:

```
java -jar target/spring-password-0.1.0.jar -v
```

## Python

We can also use the Python `bcrypt` library to achieve the same result.

### Install

```
pip install bcrypt
```

### Usage

Encode a password:

```
./pwd.py -p emma
```

Verify a password with a given hash:

```
./vpwd.py -p emma -h `./pwd.py -p emma`
```

Verify a password from the database:

```
./vpwd.py -p user1 -h '{bcrypt}$2a$10$klUL8FE6toRTHKx/ZUMUY.xdBPZHjEmqAjGQj31ndrfbrYhm8xnNK'
```

### TODO

1. Printing INCEPTION's `{bcrypt}` prefix should be optional.
1. The Spring Framework's default cost factor of 10 is not recommended because it is considered too weak.
1. The Spring Framework's choice to use `a2` means it cannot handle long passwords.

