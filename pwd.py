#!/usr/bin/env python3

import bcrypt
import sys, getopt

from codecs import decode,encode

def usage():
  print('Usage: pwd.py [-x] -p <passwd>')

def main(argv):
  passwd = ''
  try:
    opts, args = getopt.getopt(argv,"p:x",["pwd=","help"])
  except getopt.GetoptError as err:
    print(err)
    usage()
    sys.exit(2)
  for opt, arg in opts:
    if opt in ("-x", "--help"):
      usage()
      sys.exit()
    elif opt in ("-p", "--pwd"):
      passwd = encode(arg, 'utf-8')
  if passwd == '':
    usage()
    sys.exit(2)

  salt = bcrypt.gensalt(prefix=b'2a',rounds=10)
  hashed = bcrypt.hashpw(passwd, salt)

  print("{{bcrypt}}{}".format(decode(hashed)))

if __name__ == "__main__":
   main(sys.argv[1:])
