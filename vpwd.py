#!/usr/bin/env python3

import bcrypt
import sys, getopt

from codecs import decode,encode

def usage():
  print('Usage: vpwd.py [-x] -h <hash> -p <passwd>')

# From PEP 616
def removeprefix(self: str, prefix: str, /) -> str:
    if self.startswith(prefix):
        return self[len(prefix):]
    else:
        return self[:]

# Check password against hash
def main(argv):
  passwd = ''
  hashed = ''
  try:
    opts, args = getopt.getopt(argv,"h:p:x",["hash=","pwd=","help"])
  except getopt.GetoptError as err:
    print(err)
    usage()
    sys.exit(2)
  for opt, arg in opts:
    if opt in ("-x", "--hash"):
      usage()
      sys.exit()
    elif opt in ("-h", "--hash"):
      hashed = encode(removeprefix(arg, '{bcrypt}'), 'utf-8')
    elif opt in ("-p", "--pwd"):
      passwd = encode(arg, 'utf-8')
  if passwd == '' or hashed == '':
    usage()
    sys.exit(2)

  # Debug printing parameters:
  # print("{{bcrypt}}{}".format(hashed.decode()))
  # print("{}".format(passwd.decode()))

  # Does password match?
  if bcrypt.checkpw(passwd, hashed):
    print("OK")
  else:
    print("ERR")

if __name__ == "__main__":
   main(sys.argv[1:])
